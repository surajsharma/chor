import { atom } from 'recoil';

export const updateAtom = atom({
  key: 'updatedAtom',
  default: {
    event: null,
    data: null,
  },
});
