import { atom, selectorFamily } from 'recoil';
import { getWindows, getCurrentWindow, getWindow } from '../actions';

export const windowState = selectorFamily({
  key: 'windowState',
  get:
    ({ windowId }) =>
    async ({ get }) => {
      const w = getWindow({ windowId });
      return w;
    },
});

export const selectedWindowState = atom({
  key: 'selectedWindowState',
  default: getCurrentWindow(),
});

export const windowListState = atom({
  key: 'windowListState',
  default: getWindows(),
});
