import { atom, atomFamily, selectorFamily } from 'recoil';
import { getCurrentTab, getTabsInWindow, getTab } from '../actions';

export const currentTab = atom({
  key: 'currentTab',
  default: getCurrentTab(),
});

export const tabsInWindowAtom = atomFamily({
  key: 'tabsInWindow',
  default: ({ windowId }) => getTabsInWindow({ windowId }),
});

export const tabsInWindow = selectorFamily({
  key: 'tabsInWindowState',
  get:
    ({ windowId }) =>
    async ({ get }) => {
      const oldTabs = get(tabsInWindowAtom);
      const tabs = await getTabsInWindow({ windowId });
      if (tabs) {
        return tabs;
      }
    },
});

export const tabState = selectorFamily({
  key: 'tabState',
  get:
    ({ tabId }) =>
    async ({ get }) => {
      const tab = await getTab({ tabId });
      if (tab) {
        return tab;
      }
    },
});
