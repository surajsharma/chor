import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import icon from '../assets/img/icon-128.png';

//recoil
import { currentTab, tabState } from '../state/tabs';
import { useRecoilValue } from 'recoil';
import { rgbToRgba, random_rgb, checkImage, fetchAsBlob } from '../utils/color';

export default function Tab({ windowId, tabId }) {
  const ct = useRecoilValue(currentTab);
  const t = useRecoilValue(tabState({ tabId }));

  const [colorData, setColorData] = useState(null);
  let timer;

  const onClick = () => {
    console.log('single', t.id, ct.id);
  };

  const onDoubleClick = () => {
    console.log('double', t);
  };

  const onClickHandler = (event) => {
    clearTimeout(timer);

    if (event.detail === 1) {
      timer = setTimeout(onClick, 200);
    } else if (event.detail === 2) {
      onDoubleClick();
    }
  };

  useEffect(() => {
    if (!t.url.includes('chrome://') && t.favIconUrl !== '' && t.favIconUrl) {
      // console.log(t.url);
      fetchAsBlob(t.favIconUrl).then(async (result) => {
        let cd = await checkImage({
          path: t.favIconUrl,
          blob: result,
        });
        setColorData(cd);
      });
    }
  }, [t]);

  return (
    <>
      {t && colorData ? (
        <TabCol
          bg={colorData.color}
          highlighted={t.highlighted}
          onClick={onClickHandler}
          active={t.id === ct.id}
        >
          <Favicon bg={colorData.color}>
            <img src={colorData.path} alt="favicon" className="f" />
            {t.pinned && <div className="e">📌</div>}

            {(t.mutedInfo.muted && <div className="e">🔇</div>) ||
              (t.audible && <div className="e">🔈</div>)}
          </Favicon>
        </TabCol>
      ) : (
        <TabCol
          bg={random_rgb()}
          highlighted={t.highlighted}
          onClick={onClickHandler}
          active={t.id === ct.id}
        >
          <Favicon bg={random_rgb()}>
            <img src={t.favIconUrl || icon} alt="favicon" className="f" />
            {t.pinned && <div className="e">📌</div>}

            {(t.mutedInfo.muted && <div className="e">🔇</div>) ||
              (t.audible && <div className="e">🔈</div>)}
          </Favicon>
        </TabCol>
      )}
    </>
  );
}

const TabCol = styled.button`
  border: 0;
  outline: 0;
  transition: box-shadow 0.15s;

  border-radius: 5px;
  box-shadow: 4px 3px 0px 1px rgba(15, 15, 15, 0.15);

  display: flex;
  align-content: center;
  align-items: center;

  margin: 5px;
  padding: 5px;

  &:hover {
    background: ${(props) => rgbToRgba(props.bg, 0.3)};
    box-shadow: 4px 3px 0px 1px ${(props) => rgbToRgba(props.bg, 0.25)};
    border-left: ${(props) => `2px dotted ${rgbToRgba(props.bg, 0.5)}`};
    cursor: pointer;
  }

  &:active {
    background: ${(props) => rgbToRgba(props.bg, 0.3)};
    box-shadow: 0px 0px 0px 1px ${(props) => rgbToRgba(props.bg, 0.1)};
    border-left: ${(props) => `2px dotted ${rgbToRgba(props.bg, 0)}`};

    cursor: pointer;
  }

  background: ${(props) =>
    props.active ? rgbToRgba(props.bg, 0.3) : 'rgba(70, 70, 70, 0.1)}'};

  border-left: ${(props) =>
    props.highlighted
      ? `2px dotted ${rgbToRgba(props.bg, 0.5)}`
      : '2px dotted rgba(4, 4, 4, 0.4)'};

  img {
    width: 24px;
    height: 24px;
    background: ${(props) => rgbToRgba(props.bg, 0.05)};
    border: ${(props) => `1px outset ${rgbToRgba(props.bg, 0.05)}}`};
  }

  .f {
    border-radius: 15px;
    margin: 5px;
  }

  .e {
    margin: 10px;
    font-size: 10px;

    display: flex;
    align-items: center;
    justify-content: center;
  }
`;

const Favicon = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
`;
