import React from 'react';
import { useRecoilValue } from 'recoil';
import { windowListState } from '../state/windows';
import Window from './Window';

export default function Windows() {
  const windows = useRecoilValue(windowListState).map((w) => w.id);
  return windows
    ? windows.map((id) => {
        return <Window id={id} key={id} />;
      })
    : null;
}
