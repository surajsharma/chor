import React from 'react';
import styled from 'styled-components';
import Windows from './Windows';

export default function ChorContainer() {
  return (
    <Chor>
      <Windows />
    </Chor>
  );
}

const Chor = styled.div`
  position: relative;

  overflow-x: hidden;
  overflow-y: auto;

  display: flex;
  flex-direction: column;
  align-items: center;

  margin-top: -40px;
  height: 100vh;

  padding: 0;
`;
