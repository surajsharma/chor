import React, { useEffect } from 'react';
import styled from 'styled-components';

import { useRecoilValue, useRecoilState } from 'recoil';
import { selectedWindowState, windowState } from '../state/windows';
import { tabsInWindowAtom } from '../state/tabs';

import Tab from './Tab';

export default function Window({ id }) {
  const w = useRecoilValue(windowState({ windowId: id }));
  const sw = useRecoilValue(selectedWindowState);

  const [tabsInWindow, setTabs] = useRecoilState(
    tabsInWindowAtom({ windowId: id })
  );

  const tabs = tabsInWindow.map((t) => t.id);

  const checkAudible = (e) => e.audible;
  const checkMuted = (e) => e.mutedInfo.muted;

  const isAudible = w.tabs ? w.tabs.some(checkAudible) : null;
  const isMuted = w.tabs ? w.tabs.some(checkMuted) : null;
  const active = w !== undefined && sw !== undefined ? w.id === sw.id : false;

  useEffect(() => {
    // console.log('window re-rendered', id);

    var port = chrome.runtime.connect({ name: 'window' });

    if (port) {
      // port.postMessage({ joke: 'Knock knock' });

      port.onMessage.addListener(function (msg) {
        switch (msg.msg) {
          case 'new_tab_created':
            if (msg.data.tab.windowId === id) {
              console.log(msg, tabsInWindow);
              setTabs([...tabsInWindow, msg.data.tab]);
            }
            break;
          default:
            break;
        }
        // if (msg.question === "Who's there?")
        // port.postMessage({ answer: 'Madame' });
        // else if (msg.question === 'Madame who?')
        // port.postMessage({ answer: 'Madame... Bovary' });
      });
    }
  }, []);

  return (
    <WindowContainer active={active}>
      <div className="col">
        <TabContainer>
          {tabs &&
            tabs.map((t, i) => <Tab windowId={w.id} tabId={t} key={t} />)}
        </TabContainer>
        <div className="col">
          <div className="windowinfo">
            <div className="bwintitle">
              <div className="name">{w.id}</div>
              <div className="audio">
                <span>{isAudible && '🔈'}</span>
                <span>{isMuted && '🔇'}</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </WindowContainer>
  );
}

const TabContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-wrap: wrap;
  border-radius: 5px;
  margin-bottom: 10px;
  margin-top: 10px;
`;

const WindowContainer = styled.div`
  .col {
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: space-between;
  }

  &:nth-child(1) {
    margin-top: 60px;
  }

  &:last-child {
    margin-bottom: 1000px;
  }

  &:hover {
    background: rgba(0, 0, 0, 0.4);
    cursor: pointer;
  }

  background: rgba(34, 34, 34, 0.3);

  backdrop-filter: blur(4px);

  padding: 50;
  display: flex;

  flex-direction: column;
  justify-content: space-between;
  align-content: center;

  width: 98.9%;
  margin-top: 10px;

  border-top: 1px solid rgba(105, 105, 105, 0.2);
  border-left: 1px solid rgba(105, 105, 105, 0.2);

  border: 1px solid
    ${(props) =>
      props.active ? 'rgba(0, 200, 255, 0.1)' : 'rgba(105, 105, 105, 0.2)'};

  border-left: 1px solid
    ${(props) =>
      props.active ? 'rgba(0, 200, 255, 0.2)' : 'rgba(105, 105, 105, 0.2)'};

  border-top: 1px solid
    ${(props) =>
      props.active ? 'rgba(0, 200, 255, 0.2)' : 'rgba(105, 105, 105, 0.2)'};

  border-radius: 5px;

  .windowinfo {
    height: 100%;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
    align-text: center;
  }

  .bwintitle {
    &:hover {
      background: linear-gradient(
        90deg,
        rgba(0, 200, 255, 0.1) 0%,
        rgba(0, 200, 255, 0.2) 100%
      );
      cursor: pointer;
    }

    font-family: sans-serif;
    text-align: right;
    font-size: 11px;

    display: flex;
    flex-direction: column;

    justify-content: flex-start;
    align-items: center;
    align-text: center;

    height: 100%;

    border-radius: 5px;
    width: 25px;

    border-top: 1px solid
      ${(props) => (props.active ? 'rgba(0, 200, 255, 0.02)' : 'transparent')};

    border-right: 4px solid
      ${(props) => (props.active ? 'rgba(0, 200, 255, 0.02)' : 'transparent')};

    background: ${(props) =>
      props.active
        ? 'linear-gradient(90deg, rgba(0,200,255,0.05) 0%, rgba(0,200,255,0.2) 100%)'
        : 'transparent'};
  }

  .name {
    display: flex;
    flex-direction: column;

    justify-content: center;
    align-items: center;
    align-text: center;

    padding: 2px;
    color: ${(props) => (props.active ? 'teal' : 'gray')};
  }

  .audio {
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
    align-text: center;
  }

  .status {
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
    align-text: center;
    padding: 2px;
  }
`;
