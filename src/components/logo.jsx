import { Online } from 'react-detect-offline';
import horus from '../assets/img/horus.png';
import React from 'react';
import styled from 'styled-components';
import { keyframes } from 'styled-components';

export default function Logo({ ver }) {
  const online = window.navigator.onLine;

  return (
    <LogoContainer>
      <Horus>
        <img src={horus} width={40} height={40} alt="" />
        <Online>{online && <Eye />}</Online>
      </Horus>
      <Brand>
        <h1>
          CHOR!
          <sup>{ver}</sup>
        </h1>
      </Brand>
    </LogoContainer>
  );
}

const LogoContainer = styled.div`
  margin-left: 5px;

  display: flex;
  align-items: center;
  justify-content: flex-start;

  background: linear-gradient(to top, transparent, rgba(200, 100, 0, 0.8));
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;

  font-family: 'East Sea Dokdo', cursive;
  text-shadow: 0px 0px 0px rgba(190, 190, 0, 0.8);

  width: 100%;
`;

const Horus = styled.div`
  margin-top: -10px;
  color: white;
`;

const ripple = keyframes`
  20% {
    box-shadow: 0px 0px 0px 6px #030e17,
      0px 0px 0px 8px rgba(200, 0, 0, 0.5);
    opacity: 0.5;
    background: black;
  }

  50% {
    box-shadow: 0px 0px 0px 6px #030e17, 0px 0px 0px 8px #222,
      0px 0px 0px 14px #030e17, 0px 0px 2px 16px rgba(200, 0, 0, 0.5);
    opacity: 0.3;
    background: orange;
  }

  100% {
    box-shadow: 0px 0px 0px 6px #030e17, 0px 0px 0px 8px transparent,
      0px 0px 0px 14px transparent, 0px 0px 2px 16px transparent,
      0px 0px 0px 22px transparent, 0px 0px 5px 24px transparent;
    opacity: 0;
    background: red;

  }`;

const Eye = styled.div`
  width: 3px;
  height: 3px;
  border-radius: 20px;
  margin-top: -36px;
  margin-left: 13px;
  border: 1px solid black;
  background: transparent;
  animation: ${ripple} 1s linear infinite;
  opacity: 0.5;
`;

const Brand = styled.div`
  margin-left: -25px;
  margin-top: 20px;
  h1 {
    display: flex;
    flex-direction: column;
    font-size: 40px;

    sup {
      margin-top: -20px;
      font-size: 14px;
      text-shadow: 0px 0px 0px rgba(255, 100, 0, 0.6);
    }
  }
`;
