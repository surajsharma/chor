import React from 'react';

export default function Search({ inputRef }) {
  let tip = 'Tap ⌘ to enter command/search mode';

  return (
    <div className="fuzzySearch">
      <input
        type="text"
        placeholder={tip}
        onFocus={(e) => (e.target.placeholder = '')}
        onBlur={(e) => (e.target.placeholder = tip)}
        ref={inputRef}
      />
    </div>
  );
}
