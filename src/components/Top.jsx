import styled from 'styled-components';

const Top = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;

  color: white;
  cursor: pointer;
  height: 40px;

  .logo {
    width: 100%;
  }

  .menu {
    filter: invert(80%) sepia(0%) saturate(132%) hue-rotate(180deg)
      brightness(0%) contrast(100%);

    display: flex;

    justify-content: right;
    z-index: 1;
    padding: 5px;
  }
`;

export default Top;
