import React from 'react';
import Ping from './ping';

export default function Footer({ title, pages, currentPage, branding }) {
  let trimmedTitle = title.length > 35 ? title.slice(0, 30) + ' ...' : title;

  return (
    <div className="footer">
      <Ping pages={pages} currentPage={currentPage} />
      <div className="titleOrBranding">{trimmedTitle || branding}</div>
    </div>
  );
}
