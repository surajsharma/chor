import settingsIcon from '../assets/img/tune_white_24dp.svg';
import React from 'react';

export default function Settings({ reload }) {
  return (
    <div className="menu" onClick={reload}>
      <img src={settingsIcon} alt="menu" />
    </div>
  );
}
