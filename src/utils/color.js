export const fetchAsBlob = (url) =>
  fetch(url).then((response) => response.blob());

export const rgbToRgba = (rgb, alpha) => {
  let rgba = rgb.slice(3, rgb.length);
  let location = rgba.length - 1;
  let output = ['rgba', rgba.slice(0, location), `,${alpha})`].join('');
  return output;
};

export const random_rgb = () => {
  var o = Math.round,
    r = Math.random,
    s = 255;
  return 'rgb(' + o(r() * s) + ',' + o(r() * s) + ',' + o(r() * s) + ')';
};

export const checkImage = ({ path, blob }) => {
  // console.log(
  //   '🔦 ~ file: color.js ~ line 19 ~ checkImage ~ path, blob',
  //   path,
  //   blob
  // );
  if (path) {
    let p = path;

    return new Promise(async (resolve) => {
      const img = new Image();

      img.onerror = () => resolve({ path: p, status: 'error' });
      img.src = URL.createObjectURL(blob);

      img.crossOrigin = 'Anonymous';

      img.onload = async () => {
        const { canvas, context } = await drawImage(p);
        let color;

        if (p.includes('.svg')) {
          color = await calculateResult(canvas, context);
          resolve({ path: p, status: 'ok', color });
        } else {
          color = await getDominantColor(canvas, path, blob);

          if (color) {
            if (color.every((color) => color === '0')) {
              color = random_rgb();
              resolve({ path: p, status: 'ok', color });
            }

            const rgb = `rgb(${color[0]},${color[1]},${color[2]})`;
            resolve({ path: p, status: 'ok', color: rgb });
          } else {
            let colorDataWithCors = await fetchAsBlob(
              'https://www.google.com/s2/favicons?domain=' + path
            ).then(async (result) => {
              return await checkImage({
                path: 'https://www.google.com/s2/favicons?domain=' + path,
                blob: result,
              });
            });
            resolve({ path: p, status: 'ok', color: colorDataWithCors.color });
          }
        }
      };
    });
  }
};

const drawImage = async (img_path) => {
  let canvas = document.createElement('canvas');
  canvas.src = img_path;
  const context = canvas.getContext('2d');

  const img = await loadImage(img_path);
  canvas.width = img.width;
  canvas.height = img.height;
  context.drawImage(img, 0, 0);

  return { canvas, context };
};

function loadImage(img_path) {
  return new Promise((r) => {
    let i = new Image();

    i.onload = () => r(i);
    i.src = img_path;
  });
}

function getDominantColor(aImg, path, blob) {
  let canvas = document.createElement('canvas');
  canvas.height = aImg.height;
  canvas.width = aImg.width;

  let context = canvas.getContext('2d');
  context.drawImage(aImg, 0, 0);

  // keep track of how many times a color appears in the image
  let colorCount = {};
  let maxCount = 0;
  let dominantColor = '';
  let data;

  try {
    // data is an array of a series of 4 one-byte values representing the rgba values of each pixel
    data = context.getImageData(0, 0, aImg.height, aImg.width).data;
  } catch (error) {
    if (error.code === 18) {
      return;
    }
  }
  for (let i = 0; i < data.length; i += 4) {
    // ignore transparent pixels
    if (data[i + 3] === 0) continue;

    let color = data[i] + ',' + data[i + 1] + ',' + data[i + 2];
    // ignore white
    if (color === '255,255,255') continue;

    colorCount[color] = colorCount[color] ? colorCount[color] + 1 : 1;

    // keep track of the color that appears the most times
    if (colorCount[color] > maxCount) {
      maxCount = colorCount[color];
      dominantColor = color;
    }
  }

  let rgb = dominantColor.split(',');
  return rgb;
}

const calculateResult = (canvas, context) => {
  let store = {};
  const imgData = context.getImageData(0, 0, canvas.width, canvas.height);
  const data = imgData.data;

  const total_pixels = canvas.width * canvas.height;
  const coverage = total_pixels;

  const max_pixel_index = total_pixels - 1;

  for (let i = 0; i < coverage; ++i) {
    const x = getPixelIndex(Math.floor(Math.random() * max_pixel_index));
    const key = `${data[x]},${data[x + 1]},${data[x + 2]}`;
    const val = store[key];
    store[key] = val ? val + 1 : 1;
  }
  const rgb_code = Object.keys(store).reduce((a, b) =>
    store[a] > store[b] ? a : b
  );
  console.log(
    '🚀 ~ file: color.js ~ line 147 ~ calculateResult ~ rgb_code',
    rgb_code
  );
  return `rgb(${rgb_code})`;
};

function getPixelIndex(numToRound) {
  //Each pixel is 4 units long: r,g,b,a
  const remainder = numToRound % 4;
  if (remainder === 0) return numToRound;
  return numToRound + 4 - remainder;
}
