import { selectedWindowState, windowListState } from '../state/windows';
import { currentTab } from '../state/tabs';
import { getRecoil, setRecoil } from 'recoil-nexus';

// TODO: Stop other window rerender on new/close tab with separate event listeners for t/w and atoms
// TODO: put color in tab, update with changes here so rerenders don't flash like random lights
// TODO: breaking if no ico found eg: https://feather.id/?ref=producthunt

// export const bgMessageHandler = async (request, sender, sendResponse) => {
//   //  window actions
//   if (request.msg === 'window_created') {
//     try {
//       const windows = getRecoil(windowListState);
//       const newWindows = [...windows, request.data.window];

//       setRecoil(windowListState, newWindows);
//       setRecoil(selectedWindowState, request.data.window);
//     } catch (error) {
//       throw error;
//     }
//   }

//   if (request.msg === 'window_closed') {
//     try {
//       const windows = getRecoil(windowListState);
//       const newWindows = windows.filter((w) => w.id !== request.data.id);
//       setRecoil(windowListState, newWindows);
//     } catch (error) {
//       throw error;
//     }
//   }

//   // tab actions
//   if (request.msg === 'focus_changed') {
//     try {
//       const windows = getRecoil(windowListState);

//       const windowSelected = windows.filter(
//         (w) => w.id === request.data.windowId
//       )[0];

//       if (windowSelected) {
//         const hTab = windowSelected.tabs.filter((t) => t.highlighted)[0];
//         setRecoil(selectedWindowState, windowSelected);
//         setRecoil(currentTab, hTab);
//       }
//     } catch (error) {
//       throw error;
//     }
//   }

//   if (request.msg === 'tab_selected') {
//     try {
//       const windows = getRecoil(windowListState);

//       let windowId;
//       let newWindows;
//       let sw;
//       let hTab;

//       for (let i = 0; i < windows.length; i++) {
//         for (let j = 0; j < windows[i].tabs.length; j++) {
//           if (windows[i].tabs[j].id === request.data.tabId) {
//             windowId = windows[i].id;
//           }
//         }
//       }

//       if (windowId) {
//         sw = windows.filter((w) => w.id === request.data.windowId)[0];
//         hTab = sw.tabs.filter((t) => t.id === request.data.tabId)[0];

//         newWindows = windows.map((w) => {
//           if (w.id === windowId) {
//             return {
//               ...w,
//               tabs: w.tabs.map((t) => {
//                 if (t.id === request.data.tabId) {
//                   return { ...t, active: true, highlighted: true };
//                 }
//                 return { ...t, active: false, highlighted: false };
//               }),
//             };
//           }
//           return w;
//         });
//       }

//       setRecoil(windowListState, newWindows);
//       setRecoil(selectedWindowState, sw);
//       setRecoil(currentTab, hTab);
//     } catch (error) {
//       throw error;
//     }
//   }

//   if (request.msg === 'tab_closed') {
//     try {
//       const windows = getRecoil(windowListState);
//       let windowSelected;

//       const newWindows = windows.map((w) => {
//         if (w.id === request.data.removeInfo.windowId) {
//           windowSelected = w;
//           return {
//             ...w,
//             tabs: w.tabs.filter((t) => t.id !== request.data.tabId),
//           };
//         }
//         return w;
//       });

//       const hTab = windowSelected.tabs.filter((t) => t.highlighted)[0];
//       setRecoil(windowListState, newWindows);
//       setRecoil(selectedWindowState, windowSelected);
//       setRecoil(currentTab, hTab);
//     } catch (error) {
//       throw error;
//     }
//   }

//   if (request.msg === 'new_tab_created') {
//     try {
//       const windows = getRecoil(windowListState);
//       const windowId = request.data.tab.windowId;
//       const tab = request.data.tab;

//       const newWindows = windows.map((w) => {
//         if (w.id === windowId) {
//           return {
//             ...w,
//             tabs: w.tabs ? [...w.tabs, tab] : [tab],
//           };
//         }
//         return w;
//       });

//       setRecoil(windowListState, newWindows);
//       setRecoil(currentTab, tab);
//     } catch (error) {
//       throw error;
//     }
//   }

//   if (request.msg === 'tab_updated') {
//     try {
//       const windows = getRecoil(windowListState);

//       let windowId;
//       let newWindows;
//       let sw;
//       let hTab;

//       for (let i = 0; i < windows.length; i++) {
//         for (let j = 0; j < windows[i].tabs.length; j++) {
//           if (windows[i].tabs[j].id === request.data.tabId) {
//             windowId = windows[i].id;
//           }
//         }
//       }

//       if (windowId) {
//         sw = windows.filter((w) => w.id === windowId)[0];
//         hTab = sw.tabs.filter((t) => t.id === request.data.tabId)[0];

//         newWindows = windows.map((w) => {
//           if (w.id === windowId) {
//             return {
//               ...w,
//               tabs: w.tabs.map((t) => {
//                 if (t.id === request.data.tabId) {
//                   return { ...t, ...request.data.changeInfo };
//                 }
//                 return t;
//               }),
//             };
//           }
//           return w;
//         });
//       }

//       setRecoil(windowListState, newWindows);
//       setRecoil(selectedWindowState, sw);
//       setRecoil(currentTab, hTab);
//     } catch (error) {
//       throw error;
//     }
//   }
// };
