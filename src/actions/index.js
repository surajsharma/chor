export function getCurrentTab() {
  let ct = chrome.tabs.query({ active: true, currentWindow: true });
  return ct;
}

export function getWindows() {
  let w = chrome.windows.getAll({ populate: true });
  return w;
}

export function getCurrentWindow() {
  let w = chrome.windows.getCurrent({ populate: true });
  return w;
}

export function getTabsInWindow({ windowId }) {
  let tiw = chrome.tabs.query({ windowId });
  return tiw;
}

export function getTab({ tabId }) {
  let t = chrome.tabs.get(tabId);
  return t;
}

export function getWindow({ windowId }) {
  const w = chrome.windows.get(windowId);
  return w;
}

//chrome control functions
//========================

//chrome audio functions
//=======================

export const muteTab = async (t) => {
  await chrome.tabs.update(t.id, { muted: !t.mutedInfo.muted });
};

export const muteEverything = (windows) => {
  if (windows.length) {
    for (let index = 0; index < windows.length; index++) {
      const w = windows[index];

      for (let index = 0; index < w.tabs.length; index++) {
        const t = w.tabs[index];
        muteTab(t);
      }
    }
  }
};
