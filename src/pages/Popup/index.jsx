import React from 'react';
import { render } from 'react-dom';
import { RecoilRoot } from 'recoil';
import RecoilNexus from 'recoil-nexus';

import Popup from './Popup';
// import './index.css';
window.addEventListener('storage', () => {
  console.log('LSC');
});

render(
  <RecoilRoot>
    <RecoilNexus />
    <Popup />
  </RecoilRoot>,
  window.document.querySelector('#app-container')
);

if (module.hot) module.hot.accept();
