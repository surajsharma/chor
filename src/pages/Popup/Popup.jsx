import React, { useEffect, Suspense } from 'react';

// components
import Top from '../../components/top';
import Logo from '../../components/logo';
import Settings from '../../components/settings';
import ChorContainer from '../../components/ChorContainer';

//machines
import { chor } from '../../machines/chor.machine';
import { useMachine } from '@xstate/react';

//styles
import './Popup.css';

//other
const Popup = () => {
  let v = '0.0.2';
  const [current, send] = useMachine(chor);

  return (
    <>
      <Top>
        <Logo ver={v} />
        <Settings reload={() => send('LOAD')} />
        {current.value}
      </Top>
      <Suspense fallback={'Loading...'}>
        <ChorContainer />
      </Suspense>
    </>
  );
};

export default Popup;
