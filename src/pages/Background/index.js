import { interpret } from 'xstate';
import { chor } from '../../machines/chor.machine';

const service = interpret(chor).onTransition((state) => {
  console.log('🔫', state.value);
});

service.start();
console.log('🔫 BACKGROUND LOADED, FSM LOADED');

chrome.runtime.onConnect.addListener(function (port) {
  console.assert(port.name === 'window');
  port.onMessage.addListener(function (msg) {
    // if (msg.joke === 'Knock knock')
    //   port.postMessage({ question: "Who's there?" });
    // else if (msg.answer === 'Madame')
    //   port.postMessage({ question: 'Madame who?' });
    // else if (msg.answer === 'Madame... Bovary')
    //   port.postMessage({ question: "I don't get it." });
  });

  chrome.tabs.onCreated.addListener((tab) => {
    // console.log('tab created', tab);
    port.postMessage({
      msg: 'new_tab_created',
      data: {
        tab,
      },
    });
  });
});

//window event listeners
//======================

chrome.windows.onFocusChanged.addListener((windowId) => {
  // console.log('🚀 focus window', windowId);
  chrome.runtime.sendMessage({
    msg: 'focus_changed',
    data: {
      windowId,
    },
  });
});

chrome.windows.onRemoved.addListener((id) => {
  // console.log(id, 'window closed');
  chrome.runtime.sendMessage({
    msg: 'window_closed',
    data: {
      id,
    },
  });
});

chrome.windows.onCreated.addListener((window) => {
  // console.log(window, 'window created');
  chrome.runtime.sendMessage({
    msg: 'window_created',
    data: {
      window,
    },
  });
});

//tab event listeners
//===================

chrome.tabs.onActivated.addListener((activeTab) => {
  // console.log(activeTab, 'active tab changed/created/destroyed');
});

chrome.tabs.onAttached.addListener((tabId, attachedInfo) => {
  // console.log(tabId, attachedInfo, 'tab attached');
});

chrome.tabs.onDetached.addListener((tabId, detachedInfo) => {
  // console.log(tabId, detachedInfo, 'tab detached');
});

chrome.tabs.onCreated.addListener((tab) => {
  // console.log('tab created', tab);
  chrome.runtime.sendMessage({
    msg: 'new_tab_created',
    data: {
      tab,
    },
  });
});

chrome.tabs.onHighlighted.addListener((tab) => {
  // console.log(tab.tabIds[0], 'tab selected');
  chrome.runtime.sendMessage({
    msg: 'tab_selected',
    data: {
      tabId: tab.tabIds[0],
      windowId: tab.windowId,
    },
  });
});

chrome.tabs.onMoved.addListener((tabId, moveInfo) => {
  // console.log(tabId, moveInfo, 'tab moved within window');
  chrome.runtime.sendMessage({
    msg: 'tab_moved',
    data: {
      tabId,
      moveInfo,
    },
  });
});

chrome.tabs.onRemoved.addListener((tabId, removeInfo) => {
  // console.log(tabId, removeInfo, 'tab closed');
  chrome.runtime.sendMessage({
    msg: 'tab_closed',
    data: {
      tabId,
      removeInfo,
    },
  });
});

chrome.tabs.onUpdated.addListener((tabId, changeInfo) => {
  // console.log(tabId, changeInfo, 'tab updated');
  chrome.runtime.sendMessage({
    msg: 'tab_updated',
    data: {
      tabId,
      changeInfo,
    },
  });
});
