// reload after editing

import { createMachine } from 'xstate';

export const chor = createMachine(
  {
    id: 'chor',
    initial: 'idle',
    context: {
      event: '',
      data: {},
    },
    states: {
      idle: {
        on: {
          update: {
            actions: ['UPDATE'],
            target: 'idle',
          },
        },
      },
      error: {
        target: 'idle',
        always: {
          actions: ['LOG'],
          target: 'idle',
        },
      },
    },
  },
  {
    actions: {
      LOG: (ctx, evt) => console.log('🚜 LOG:', evt),
      UPDATE: (ctx, evt) => {
        console.log('🧲 UPDATE: ', evt);

        switch (evt.event) {
          case 'new tab':
            console.log('🧮 new tab', evt.tab);
            break;
          case 'window changed':
            console.log(`🎟 {focus: ${evt.id}}`);
            break;
          default:
            break;
        }
      },
    },
  }
);
